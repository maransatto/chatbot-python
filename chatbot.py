from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer # método de treinar o chatbot

bot = ChatBot('Maranbot')

trainer = ListTrainer(bot)

conversa = open('chats.txt', 'r').readlines()
trainer.train(conversa)

while True:
    message = input('You:')
    if message.strip()!= 'tchau':
     reply = bot.get_response(message)
    print('ChatBot:',reply)
    if message.strip()=='tchau':
        print('ChatBot:tchau')
        break